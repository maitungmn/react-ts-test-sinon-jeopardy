import React from "react";
import { Provider } from "react-redux";
import configureStore from "./state";
import Routes from './router';

const initialState = (window as any).initialReduxState;
const store = configureStore(initialState);

const App: React.FC = () => {
  return (
    <Provider store={store}>
      <Routes />
    </Provider>
  );
};
export default App;

