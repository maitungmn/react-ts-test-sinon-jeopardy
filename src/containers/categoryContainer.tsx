import React from 'react'
import { Link } from 'react-router-dom'
import { useSelector } from 'react-redux'
import axios from 'axios'
import { IApplicationState } from '../state/ducks'
import { IServerResponse } from './categoriesContainer'
import { IClue } from '../state/ducks/app/types'

import Clues from '../components/clues'

const Category: React.FC = () => {

  const stateToProps = useSelector(({ category }: IApplicationState) => category)

  const [clues, setClues] = React.useState<IClue[]>([])

  React.useEffect(() => {
    axios.get(`https://jservice.io/api/clues?category=${stateToProps.id}`)
      .then(({ data }: IServerResponse<IClue[]>) => {
        setClues(data)
      })
  }, [setClues, stateToProps.id])

  return (
    <div>
      <Link to="/" className="link-home"><h4>Home</h4></Link>
      <h2>{stateToProps.title}</h2>
      <Clues clues={clues} />
    </div>
  )
}

export default Category