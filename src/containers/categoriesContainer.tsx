import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from 'react-router-dom'
import axios from 'axios';
import { ICategory, ApiResponse } from '../state/ducks/app/types'
import { setCategories, pickCategory } from "../state/ducks/app/actions";
import { IApplicationState } from "../state/ducks";

export interface IServerResponse<T> extends ApiResponse {
  data: T
}

const CategoriesContainer: React.FC = () => {
  const dispatch = useDispatch();

  const stateToProps = useSelector(
    ({ categories }: IApplicationState) => categories
  );

  const categories: ICategory[] = stateToProps.data

  React.useEffect(() => {
    if (!categories.length) {
      axios.get('https://jservice.io/api/categories?count=20')
        .then(({ data }: IServerResponse<ICategory[]>) => {
          dispatch(setCategories(data))
        })
    }
  }, [dispatch, categories])


  return (
    <div>
      <h2>Jeopardy!</h2>
      {categories.map((i: ICategory, index: number) => (
        <div key={index}>
          <Link to="/category" onClick={() => dispatch(pickCategory(i))}><h4>{i.title}</h4></Link>
        </div>
      ))}
    </div>

  );
};
export default CategoriesContainer;
