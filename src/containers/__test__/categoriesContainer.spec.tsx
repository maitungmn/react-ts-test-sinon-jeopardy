import { mount } from "enzyme";
import React from "react";
import { Provider } from 'react-redux';
import { Router } from 'react-router-dom';
import configureStore from "redux-mock-store";
import history from '../../history'
import { ICategoriesState } from "../../state/ducks/app/types";
import CategoriesContainer from "../categoriesContainer";
import fixtures from '../../state/ducks/app/__tests__/__mockData__/fixtures.json'

const mockStore = configureStore();
type TProps = {
  categories: ICategoriesState
}
const props: TProps = { categories: { data: fixtures.categories } }

describe('Categories container', () => {
  const wrapper = mount(
    <Provider store={mockStore(props)}>
      <Router history={history}>
        <CategoriesContainer />
      </Router>
    </Provider>
  );

  const categoriesWrapper = wrapper.find(CategoriesContainer)

  it('renders the title', () => {
    expect(categoriesWrapper.find('h2').text()).toEqual('Jeopardy!')
  });

  it('creates the correct number of links', () => {
    expect(categoriesWrapper.find('Link').length).toEqual(fixtures.categories.length)
  })

  it('title the links correctly', () => {
    categoriesWrapper.find('Link h4').forEach((linkTitle, index) => {
      expect(linkTitle.text()).toEqual(fixtures.categories[index].title)
    })
  })
});

