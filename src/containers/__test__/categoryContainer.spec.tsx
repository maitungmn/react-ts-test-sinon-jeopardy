import { mount } from "enzyme";
import React from "react";
import { Provider } from 'react-redux';
import { Router } from 'react-router-dom';
import configureStore from "redux-mock-store";
import history from '../../history'
import { ICategory } from "../../state/ducks/app/types";
import CategoryContainer from "../categoryContainer";
import fixtures from '../../state/ducks/app/__tests__/__mockData__/fixtures.json'
import { fakeServer, SinonFakeServer } from 'sinon'
import { Server } from "http";

const mockStore = configureStore();
type TProps = {
  category: ICategory
}
const props: TProps = { category: fixtures.categories[0] }

describe('Categories container', () => {
  let server: SinonFakeServer;

  beforeEach(() => {
    server = fakeServer.create()

    server.respondWith(
      'GET',
      `https://jservice.io/api/clues?category=${fixtures.categories[0].id}`,
      [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify(fixtures.clues)
      ]
    )
  })

  describe('when creating a new category', () => {
    let wrapper: any;
    beforeEach((done: jest.DoneCallback) => {
      wrapper = mount(
        <Provider store={mockStore(props)}>
          <Router history={history}>
            <CategoryContainer />
          </Router>
        </Provider>
      );

      server.respond()

      setTimeout(done)
    })

    it('logs the category', () => {
      console.log(wrapper.find('Clues').props())
    })

    it('initializes the clues in state', () => {
      expect(wrapper.find('Clues Clue').length).toEqual(fixtures.clues.length)
    })
  })
});

