import React, { lazy, Suspense } from 'react'
import { Router, Switch, Route } from 'react-router-dom'
import history from "./history";

const CategoriesContainer = lazy(() => import('./containers/categoriesContainer'))
const CategoryContainer = lazy(() => import('./containers/categoryContainer'))

const Routes: React.FC = () => {
  return (
    <Suspense fallback={<div>Loading...</div>}>
      <Router history={history}>
        <Switch>
          <Route exact path='/' component={CategoriesContainer} />
          <Route exact path='/category' component={CategoryContainer} />
        </Switch>
      </Router>
    </Suspense>
  )
}

export default Routes;