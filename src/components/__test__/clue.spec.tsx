import React from 'react'
import { shallow } from 'enzyme'
import Clue from '../clue'
import { clue } from '../../state/ducks/app/__tests__/__mockData__/fixtures.json'

describe('Clue', () => {
  const clueWrapper = shallow(<Clue {...clue} />)

  it('renders the clue value', () => {
    expect(clueWrapper.find('h4').first().text()).toEqual(clue.value.toString())
  })

  it('renders the clue question', () => {
    expect(clueWrapper.find('h5').first().text()).toEqual(clue.question)
  })

  it('renders the clue answer', () => {
    expect(clueWrapper.find('h5').at(1).text()).toEqual(clue.answer)
  })

  it('sets the answer with the `text-hidden` class', () => {
    expect(clueWrapper.find('h5').at(1).hasClass('text-hidden'))
  })

  describe('when rendering a clue with no value', () => {
    const noValue = {
      ...clue,
      value: 0
    }
    const clueNoValueWrapper = shallow(<Clue {...noValue} />)
    it('displays the value as `unknown`', () => {
      expect(clueNoValueWrapper.find('h4').text()).toEqual('unknown')
    })
  })

  describe('when clicking on the clue', () => {
    clueWrapper.simulate('click')
    it('sets the answer with the `text-revealed` class', () => {
      expect(clueWrapper.find('h5').at(1).hasClass('text-revealed'))
    })
  })
})