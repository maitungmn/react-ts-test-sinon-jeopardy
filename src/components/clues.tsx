import React from "react";
import { IClue } from "../state/ducks/app/types";
import Clue from './clue'

type props = {
  clues: IClue[];
}

const Clues: React.FC<props> = ({ clues }: props) => {
  return (
    <div>
      <ul>
        {clues.map((clue: IClue, index: number) => (
          <Clue key={index} {...clue} />
        ))}
      </ul>
    </div>
  );
};

export default Clues;
