import React from "react";
import { IClue } from "../state/ducks/app/types";

const Clue: React.FC<IClue> = ({ answer, question, value }: IClue) => {

  const [reveal, setReaveal] = React.useState<boolean>(false)
  return (
    <div className="clue" onClick={() => setReaveal(true)}>
      <h4>{value || 'unknown'}</h4>
      <hr />
      <h5>{question}</h5>
      <hr />
      <h5 className={reveal ? 'text-revealed' : 'text-hidden'}>{answer}</h5>
    </div>
  );
};

export default Clue;
