import { ICategoriesState, ICategory } from './types';
import { ActionType, getType } from 'typesafe-actions';
import * as Actions from './actions';

export type CategoriesActions = ActionType<typeof Actions>;

export const initialState: ICategoriesState = {
  data: [],
};

export const categoriesReducer = (
  state: ICategoriesState = initialState,
  action: CategoriesActions
): ICategoriesState => {
  switch (action.type) {
    case getType(Actions.setCategories): {
      return { ...state, data: action.payload };
    }
    default:
      return state;
  }
};

const initialCategoryState: ICategory = {
  id: 0,
  title: '',
  clues_count: 0,
};

export const categoryReducer = (
  state: ICategory = initialCategoryState,
  action: CategoriesActions
): ICategory => {
  switch (action.type) {
    case getType(Actions.pickCategory): {
      return action.payload;
    }
    default:
      return state;
  }
};
