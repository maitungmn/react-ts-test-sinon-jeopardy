import { EAppActionTypes, ICategory } from './types';
import { createAction } from 'typesafe-actions';

export const setCategories = createAction(EAppActionTypes.SET_CATEGORIES)<
  ICategory[]
>();

export const pickCategory = createAction(EAppActionTypes.PICK_CATEGORY)<
  ICategory
>();
