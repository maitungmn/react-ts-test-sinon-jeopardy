export enum EAppActionTypes {
  SET_CATEGORIES = '@@app/SET_CATEGORIES',
  PICK_CATEGORY = '@@app/PICK_CATEGORY',
}

export type ApiResponse = Record<string, any>;

export interface ICategoriesState {
  data: ICategory[] | [];
}

export interface ICategory {
  id: number;
  title: string;
  clues_count: number;
}

export interface ICluesState {
  data: IClue[] | [];
}

export interface IClue {
  id: number;
  answer: string;
  question: string;
  value: number;
  airdate?: string;
}

export type ApiErrorResponse = {
  message: string;
};
