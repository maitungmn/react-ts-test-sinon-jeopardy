import { combineReducers } from 'redux';

import { categoriesReducer, categoryReducer } from './app/reducers';
import { ICategoriesState, ICategory } from './app/types';

// The top-level state object
export interface IApplicationState {
  categories: ICategoriesState;
  category: ICategory;
}

export const rootReducer = combineReducers<IApplicationState>({
  categories: categoriesReducer,
  category: categoryReducer,
});
