import { createStore, Store } from 'redux';
import { IApplicationState, rootReducer } from './ducks/index';

export default function configureStore(
  initialState: IApplicationState
): Store<IApplicationState> {
  const store = createStore(rootReducer, initialState);

  return store;
}
